#ifndef NETWORK_H
#define NETWORK_H

#include "Socket.h"


class Network : protected Socket
{
    public:
        Network();
        virtual ~Network();
        int getFactoredPrime();

    protected:
        int p = 3; // Easy prime
        int q = 11; // Easy prime
        int n = p * q; // Factored prime
        double g = 20; // g is theta?
        double alpha = 1; // Alpha is nonce, preset value is 0
        int initialNetworkPseudonym; // The initial network pseudonym is to create node initial pseudonyms for local dynamic pseudonym generation.
};

#endif // NETWORK_H
