#ifndef NODE_H
#define NODE_H

#include "Network.h"
#include "Socket.h"


class Node : protected Network
{
    public:
        Node();
        virtual ~Node();
        int createNetworkPseudonym();
        int getFactoredPrime();

    private:
        int initialPseudonym;
        int secretKey;
};

#endif // NODE_H
